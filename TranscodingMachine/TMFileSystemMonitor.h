//
//  TMFileSystemMonitor.h
//  TranscodingMachine
//
//  Created by Cory Powers on 7/15/14.
//  Copyright (c) 2014 Cory Powers. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TMFileSystemMonitor;

@protocol TMFileSystemMonitorDelegate <NSObject>
- (void)fileSystemMonitor: (TMFileSystemMonitor *)monitor scanFound: (NSArray *)files;
- (void)fileSystemMonitor: (TMFileSystemMonitor *)monitor scanFailed: (NSError *)error;
@end

@interface TMFileSystemMonitor : NSObject
@property (nonatomic, readonly, weak) id<TMFileSystemMonitorDelegate> delegate;
@property (nonatomic, readonly) NSString *monitoredPath;

- (instancetype)initWithPath: (NSString *)monitoredPath delegate: (id<TMFileSystemMonitorDelegate>)aDelegate;
- (void)scan;

@end
