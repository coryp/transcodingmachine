//
//  TMDataStore.h
//  TranscodingMachine
//
//  Created by Cory Powers on 7/15/14.
//  Copyright (c) 2014 Cory Powers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMDataStore : NSObject
+ (instancetype)sharedStore;

@end
