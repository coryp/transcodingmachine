//
//  TMDataStore.m
//  TranscodingMachine
//
//  Created by Cory Powers on 7/15/14.
//  Copyright (c) 2014 Cory Powers. All rights reserved.
//

#import "TMDataStore.h"
#import "FCModel.h"

@implementation TMDataStore
+ (instancetype)sharedStore {
    static TMDataStore *_sharedStore = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedStore = [[TMDataStore alloc] init];
    });
    
    return _sharedStore;
}

- (id)init {
    self = [super init];
    if (self) {

		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
		NSString *basePath = ([paths count] > 0) ? paths[0] : NSTemporaryDirectory();
		NSString *dbPath = [[basePath stringByAppendingPathComponent:@"TranscodingMachine"] stringByAppendingString:@"store.sqlite3"];

		
		[FCModel openDatabaseAtPath:dbPath withSchemaBuilder:^(FMDatabase *db, int *schemaVersion) {
			[db beginTransaction];
			
			// My custom failure handling. Yours may vary.
			void (^failedAt)(int statement) = ^(int statement){
				int lastErrorCode = db.lastErrorCode;
				NSString *lastErrorMessage = db.lastErrorMessage;
				[db rollback];
				NSAssert3(0, @"Migration statement %d failed, code %d: %@", statement, lastErrorCode, lastErrorMessage);
			};
			
			if (*schemaVersion < 1) {
				if (! [db executeUpdate:
					   @"CREATE TABLE TMMonitoredFile ("
					   @"    path     TEXT PRIMARY KEY,"
					   @"    size		   INTEGER NOT NULL DEFAULT 0,"
					   @"    createdAtTs       INTEGER NOT NULL DEFAULT 0,"
					   @"    updatedAtTs       INTEGER NOT NULL DEFAULT 0"
					   @");"
					   ]) failedAt(1);
				
//				if (! [db executeUpdate:@"CREATE INDEX IF NOT EXISTS path ON TMMonitoredFile (path);"]) failedAt(2);

				*schemaVersion = 1;
			}
			
//			if (*schemaVersion < 2) {
//				if (! [db executeUpdate:
//					   @"CREATE TABLE SICSocialNetwork ("
//					   @"    socialNetworkId   INTEGER PRIMARY KEY,"
//					   @"    userId            INTEGER NOT NULL DEFAULT 0,"
//					   @"    externalId        INTEGER NOT NULL DEFAULT 0,"
//					   @"    friends           INTEGER NOT NULL DEFAULT 0,"
//					   @"    network           TEXT NOT NULL DEFAULT '',"
//					   @"    username          TEXT NOT NULL DEFAULT '',"
//					   @"    avatarUrl         TEXT NOT NULL DEFAULT '',"
//					   @"    createdAtTs       INTEGER NOT NULL DEFAULT 0,"
//					   @"    updatedAtTs       INTEGER NOT NULL DEFAULT 0"
//					   @");"
//					   ]) failedAt(12);
//				
//				
//				*schemaVersion = 2;
//			}
//			
//			if (*schemaVersion < 3) {
//				if (! [db executeUpdate:
//					   @"ALTER TABLE SICCampaign ADD COLUMN signupBonus REAL NOT NULL DEFAULT 0.00;"
//					   ]) failedAt(13);
//				
//				
//				if (! [db executeUpdate:
//					   @"ALTER TABLE SICCampaign ADD COLUMN imageUrl TEXT NOT NULL DEFAULT '';"
//					   ]) failedAt(14);
//				
//				if (! [db executeUpdate:
//					   @"CREATE TABLE SICShareCode ("
//					   @"    shareCodeId   INTEGER PRIMARY KEY,"
//					   @"    campaignId            INTEGER NOT NULL DEFAULT 0,"
//					   @"    productId        INTEGER NOT NULL DEFAULT 0,"
//					   @"    userId           INTEGER NOT NULL DEFAULT 0,"
//					   @"    code           TEXT NOT NULL DEFAULT '',"
//					   @"    url          TEXT NOT NULL DEFAULT '',"
//					   @"    createdAtTs       INTEGER NOT NULL DEFAULT 0,"
//					   @"    updatedAtTs       INTEGER NOT NULL DEFAULT 0"
//					   @");"
//					   ]) failedAt(15);
//				
//				if (! [db executeUpdate:
//					   @"CREATE TABLE SICShareEvent ("
//					   @"    shareEventId     INTEGER PRIMARY KEY,"
//					   @"    type             TEXT NOT NULL DEFAULT '',"
//					   @"    shareCodeId      INTEGER NOT NULL DEFAULT 0,"
//					   @"    purchaseId       INTEGER NOT NULL DEFAULT 0,"
//					   @"    merchantId       INTEGER NOT NULL DEFAULT 0,"
//					   @"    userId           INTEGER NOT NULL DEFAULT 0,"
//					   @"    name             TEXT NOT NULL DEFAULT '',"
//					   @"    itemName              TEXT NOT NULL DEFAULT '',"
//					   @"    commission      REAL NOT NULL DEFAULT 0.00,"
//					   @"    subtotal      REAL NOT NULL DEFAULT 0.00,"
//					   @"    medium              TEXT NOT NULL DEFAULT '',"
//					   @"    createdAtTs      INTEGER NOT NULL DEFAULT 0,"
//					   @"    updatedAtTs      INTEGER NOT NULL DEFAULT 0"
//					   @");"
//					   ]) failedAt(16);
//				
//				
//				
//				*schemaVersion = 3;
//			}
						
            [db commit];
		}];
		
	}
    return self;
}

@end
