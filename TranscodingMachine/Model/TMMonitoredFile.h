//
//  TMMonitoredFile.h
//  TranscodingMachine
//
//  Created by Cory Powers on 7/15/14.
//  Copyright (c) 2014 Cory Powers. All rights reserved.
//

#import "FCModel.h"

@interface TMMonitoredFile : FCModel
@property (nonatomic, strong) NSString *path;
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, assign) NSUInteger createdAtTs;
@property (nonatomic, assign) NSUInteger updatedAtTs;

@end
