//
//  TMQueueItemCell.m
//  TranscodingMachine
//
//  Created by Cory Powers on 7/15/14.
//  Copyright (c) 2014 Cory Powers. All rights reserved.
//

#import "TMQueueItemCell.h"

@implementation TMQueueItemCell

- (void)awakeFromNib{
	[self.imageView unregisterDraggedTypes];
}
@end
