//
//  TMFileSystemMonitor.m
//  TranscodingMachine
//
//  Created by Cory Powers on 7/15/14.
//  Copyright (c) 2014 Cory Powers. All rights reserved.
//

#import "TMFileSystemMonitor.h"

@interface TMFileSystemMonitor ()
@property (nonatomic, weak) id<TMFileSystemMonitorDelegate> delegate;
@property (nonatomic, strong) NSString *monitoredPath;

@end

@implementation TMFileSystemMonitor

- (instancetype)initWithPath:(NSString *)monitoredPath delegate:(id<TMFileSystemMonitorDelegate>)aDelegate{
	self = [super init];
	if (self) {
		self.delegate = aDelegate;
		self.monitoredPath = monitoredPath;
	}
	
	return self;
}

- (void)scan{
	NSError *error;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *files = [fileManager contentsOfDirectoryAtPath:self.monitoredPath error:&error];
	if (error) {
		[self.delegate fileSystemMonitor:self scanFailed:error];
	}else{
		NSMutableArray *fullPathFiles = [NSMutableArray arrayWithCapacity:files.count];
		for (NSString *filename in files) {
			[fullPathFiles addObject:[self.monitoredPath stringByAppendingPathComponent:filename]];
		}
		[self.delegate fileSystemMonitor:self scanFound:fullPathFiles];
	}
}
@end
